package com.ast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringAzureSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringAzureSqlApplication.class, args);
	}

}
