/*
 *  05-08-2021 
 *  Copyright (c) 2021 AST Corporation. All Rights Reserved.
 *
 *
 *
*/

package com.ast;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rsuryavanshi
 *
 */
@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;

	    @PostMapping("/users")
	    public User createUser(@RequestBody User user) {
	        return userRepository.save(user);
	    }
	    

	    @GetMapping("/users")
	    public List<User> findAll() {
	    	
	        return userRepository.findAll();
	    }

}
